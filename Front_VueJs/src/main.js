import '@/plugins/vue-composition-api'
import '@/styles/styles.scss'
import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
window.axios = require('axios');
Vue.config.productionTip = false

// import plugin
import VueToastr from "vue-toastr";

Vue.use(VueToastr);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')
